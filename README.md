[![pipeline status](https://gitlab.com/mehrshad.shams/algorithms/badges/master/pipeline.svg)](https://gitlab.com/mehrshad.shams/algorithms/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

# Algorithms and data structures

# Data Structures
TBD

# Algorithms
TBD

## Problems

### Leetcode

* [Product of Array Except Self](https://github.com/mehrshadshams/algorithms/blob/master/algs4/src/main/java/com/mshams/cs/books/skienna/chapter3/ProductOfArrayExceptSelf.java)
