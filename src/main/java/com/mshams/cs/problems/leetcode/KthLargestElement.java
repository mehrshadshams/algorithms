package com.mshams.cs.problems.leetcode;

import com.mshams.cs.utils.interfaces.Complexity;
import com.mshams.cs.utils.interfaces.ComplexityLevel;
import com.mshams.cs.utils.interfaces.Tricky;

/**
 * https://leetcode.com/problems/kth-largest-element-in-an-array
 */
@Complexity(ComplexityLevel.MEDIUM)
@Tricky
public class KthLargestElement {
}
